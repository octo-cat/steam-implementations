This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

```
> npm i
```
### .env
Create a .env file which will hold the **private Reepay API key**, add this entry:
```
REACT_APP_REEPAY_KEY='...'
```
Then start the project
```
> npm start
```
---

## Reepay
You need access to you admin.reepay.com module, where you need to make sure you have already created:
 - A test plan (Configuration tab > Plans)
 - A test user (Customers tab)

Save the `handle` for customer (ex: *cust-0001* - think of it as a customerId) and the `planId` (ex: *plan-2fa77*)  for the test plan. You will need them to make requests.


### Reepay utils
The code snippet for different Reepay subscription actions is in the **src/utils/reepay**.

#### createSubscription
**Important**: Each time you create a new subscription, make sure you **change** the `body.handle` param. 

```
const  body = {
plan: 'plan-2fa95', // the planID from above
signup_method: 'link',
customer: "cust-0001", // the customer's handle from above
handle: 'sub-6'
}
```

That will be the subscription id. Otherwise it will throw an error

```
{
    "code": 11,
    "error": "Duplicate handle",
    "message": "subscription already exists",
    "path": "/v1/subscription",
    "timestamp": "2021-08-13T13:04:05.775+00:00",
    "http_status": 400,
    "http_reason": "Bad Request",
    "request_id": "7375613e693b0f87c81c16559a62dd20"
}
```

On success, this will return an object with the `handle` property. This will get passed to `getSubscriptionSession` and used for paying for that specific subscription.

This is an example object returned on success:

```
{
    "handle": "sub-5",
    "customer": "cust-0001",
    "plan": "plan-2fa95",
    "state": "active",
    "test": true,
    "quantity": 1,
    "timezone": "Europe/London",
    "created": "2021-08-13T13:08:46.797+00:00",
    "activated": "2021-08-13T13:08:46.797+00:00",
    "renewing": true,
    "plan_version": 1,
    "start_date": "2021-08-13T13:08:46.797+00:00",
    "grace_duration": 172800,
    "current_period_start": "2021-08-13T13:08:46.797+00:00",
    "next_period_start": "2021-09-13T13:08:46.797+00:00",
    "first_period_start": "2021-08-13T13:08:46.797+00:00",
    "is_cancelled": false,
    "in_trial": false,
    "has_started": true,
    "renewal_count": 1,
    "payment_method_added": false,
    "failed_invoices": 0,
    "failed_amount": 0,
    "cancelled_invoices": 0,
    "cancelled_amount": 0,
    "pending_invoices": 1,
    "pending_amount": 5000,
    "dunning_invoices": 0,
    "dunning_amount": 0,
    "settled_invoices": 0,
    "settled_amount": 0,
    "refunded_amount": 0,
    "pending_additional_costs": 0,
    "pending_additional_cost_amount": 0,
    "transferred_additional_costs": 0,
    "transferred_additional_cost_amount": 0,
    "pending_credits": 0,
    "pending_credit_amount": 0,
    "transferred_credits": 0,
    "transferred_credit_amount": 0,
    "hosted_page_links": {
        "payment_info": "https://checkout.reepay.com/#/subscription/pay/de_DE/db563a0ecfeb5e2df5294659f09862bc/sub-5"
    }
}
```
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTM1NTk4NTk0NF19
-->