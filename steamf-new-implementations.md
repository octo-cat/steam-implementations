
## Steamfoss implementations
The two new main features which need to be implemented next for Steamfoss:
 - Redesign of user profile
 - Recurring Subscription
 
I'm just writing some pointers about things to consider, things I wish I knew if jumping on this tasks, tips, and maybe how I would do it. You decide which path you take. 
You can still contact me next week if you have any doubts :)

### Redesign
Because the current layout doesn't have a 'user cars' page, I was thinking it makes sense to start with some small redesign for now. I can start a 'redesign' branch. It can just be some basic layout (hard code maybe), like creating the user cars menu + the subscription button.
Will create a *'/redesign'* route and place the new Home page there.

Here is the adobe xd link for the [new design](https://xd.adobe.com/view/d56ab683-a0a7-4ba0-8733-1aa19de7d7e5-8039/).


---
### Subscription
Here are some useful guides about [creating a Reepay subscribtion](https://docs.reepay.com/docs/new-subscription-business) and [general subscription handling](https://docs.reepay.com/docs/simple-subscription-handling).

A subscription is done for each car. When subscribing a car, the Brand + Modal + **Serial Number** should be added. Right now, we don't record  any information about a users cars. Probably we should create a **Cars** field (array) on the customers table.

#### Testing Reepay
If you want to test, you can create a new account on reepay and use your own API key. 
The **current API key** is stored on *AWS under AWS System Manager > Parameter Store* (not sure if this [link](https://eu-west-1.console.aws.amazon.com/systems-manager/parameters/?region=eu-west-1&tab=Table](https://eu-west-1.console.aws.amazon.com/systems-manager/parameters/?region=eu-west-1&tab=Table)) will take you there or not).

We're fetching the key on the serverless side [here](https://github.com/steamfoss-org/SteamFoss-Serverless-API/blob/1f10a014a1c47fad22380467e223c7e7e6029600/libs/ssm-lib.js#L6). Probably this needs a check if env is prod/dev when we're going to sepparate them and use a designated apiKey for dev. (will actually love to see how to sepparate those two envs one day when you have time).

You would have to **create the same subscription plans** there as well. This can be done in the [admin module]([https://admin.reepay.com](https://admin.reepay.com/)), at "Configuration" > "Plans" > "New Plan".

##### An example app
I also created an small app to test creating a subscription and then paying for it. You can find it at the old Steamfoss org, [@reepay-subscription-test](https://github.com/steamfoss-org/reepay-subscription-test). There's also steps on how to run it in the README file.

#### Order flow
Here is an overview of the *current* order flow:

#####  Car + Package:
```mermaid
graph LR
A[Area code] --> B[Car + Model] --> C[Wash Package] --> D[Extra Products] --> E[Package Overview] --> F[User Details]

E -- add another car/ start over --> A
```

#####  User Details:
```mermaid
graph LR
A[Select Booking Date + Time] --> B[Check/Modify User Details] --> C{Payment Option} -- Pay later ---> F((Order Confirmation))
C -- pay now --> D[Checkout] --> F
```
You can find the Order code +  steps [here](https://github.com/steamfoss-org/SteamFoss-Customer-Client/blob/4a36ecba1503a6be25cc25fc2f386d6ff68439e8/src/views/Order/Order.js#L90).

For users ordering with a subscription, the order flow will be modified (subject to modifications):


```mermaid
graph LR
A[Select Booking Date + Time] --> B[Select Extra Products] --> C((Order Confirmation))
```


I think we don't need a new design and can use the old components for creating this new order flow. But I would create a new parent component (maybe 'SubscriptionOrder', instead of using the old one '[Order](https://github.com/steamfoss-org/SteamFoss-Customer-Client/blob/main/src/views/Order/Order.js)'

The user can still choose Extra Products and will get charge for them. Some possibilities to charge for these Extra Prods:
 1. Do a Reepay checkout with the total Extra Prods price and the user can pay now / later (just like in an order)
 2. (More ideally) Add the extra products total price to the monthly subscription price. Still not sure if Reepay would allow to cutomize the this price.

For (2), a solution can be the [subscription add-ons](https://docs.reepay.com/docs/subscription-add-on#create-subscription-with-add-ons). They seem kinda rigid for our needs, but might work with a few tweaks. There might also be another posibility.

### On the admin side

The admin will be able to [create a subscription plan](https://reference.reepay.com/api/#create-plan) + manually [cancel](https://reference.reepay.com/api/#cancel-subscription)/pause subscriptions for individual users.

Also, the admin should be able to see a list of subscriptions for an individual user. Can be:
- something like the [customer table](https://www.admin.steamfoss.com/customers) but with subscription history in the dropdown, instead of a list of orders
- can also be a different page for each customer displaying their subscriptions, something like the [order detail page](https://www.admin.steamfoss.com/order/b4ab8991-d4d7-11eb-8917-79312b52edae)

Not sure which approach we're taking. Better discuss this with Lau.


<!--stackedit_data:
eyJoaXN0b3J5IjpbNDk0NDcyMTkxXX0=
-->